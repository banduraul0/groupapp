package com.example.groupapp.dataClasses

data class User(val username: String = "",
                val email: String = "",
                val password: String = "",
                val id: String = "",
                val listOfGroups: ArrayList<String> = ArrayList()
)