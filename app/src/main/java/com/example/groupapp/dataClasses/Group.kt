package com.example.groupapp.dataClasses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Group(val name: String = "",
                 val id: String = "",
                 val users: ArrayList<String> = ArrayList()
) : Parcelable