package com.example.groupapp.dataClasses

data class ListOfMessages(val messages: ArrayList<Message> = ArrayList())