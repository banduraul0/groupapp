package com.example.groupapp.dataClasses

import java.util.*

data class Message(val user: User = User(),
                   val text: String = "",
                   val time: Date = Date()
)