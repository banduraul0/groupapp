package com.example.groupapp.seeUserGroups

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R
import com.example.groupapp.dataClasses.Group
import com.example.groupapp.databinding.UserGroupsViewHolderBinding

class UserGroupsAdapter(private val clickListener: GroupsListClickListener) : RecyclerView.Adapter<UserGroupsAdapter.GroupViewHolder>() {
    private val groups: ArrayList<Group> = ArrayList()

    interface GroupsListClickListener {
        fun groupItemClicked(group: Group)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        val viewHolderBinding = DataBindingUtil.inflate<UserGroupsViewHolderBinding>(
            LayoutInflater.from(parent.context), R.layout.user_groups_view_holder, parent, false)
        return GroupViewHolder(viewHolderBinding)
    }

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        holder.init((position + 1).toString(), groups[position])
    }

    override fun getItemCount(): Int {
        return groups.size
    }

    fun setData(groupList: ArrayList<Group>) {
        groups.clear()
        groups.addAll(groupList)
        notifyDataSetChanged()
    }

    inner class GroupViewHolder(private val binding: UserGroupsViewHolderBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var group: Group

        fun init(groupNumber: String, group: Group) {
            this.group = group
            initListeners()
            initLayout(groupNumber)
        }

        private fun initListeners() {
            binding.root.setOnClickListener { clickListener.groupItemClicked(group) }
        }

        private fun initLayout(groupNumber: String) {
            binding.groupNumber.text = groupNumber
            binding.groupString.text = group.name
        }
    }
}