package com.example.groupapp.seeUserGroups

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.groupapp.dataClasses.Group
import com.example.groupapp.dataClasses.ListOfMessages
import com.example.groupapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import kotlin.collections.ArrayList

class SeeUserGroupsViewModel : ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val groupListLiveData = MutableLiveData<ArrayList<Group>>(ArrayList())
    val joinGroupStatusLiveData = MutableLiveData<Boolean>()
    val joinGroupErrorMessageLiveData = MutableLiveData<String>()
    val createGroupStatusLiveData = MutableLiveData<Boolean>()
    val createGroupErrorMessageLiveData = MutableLiveData<String>()

    fun createGroup(name: String) {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).get().addOnCompleteListener { userTask ->
            if (userTask.isSuccessful) {
                userTask.result?.let { documentSnapshot ->
                    val uniqueId = UUID.randomUUID().toString().replace("-", "")
                    val group =
                        Group(
                            name = name,
                            id = uniqueId.substring(0, 6)
                        )
                    documentSnapshot.toObject(User::class.java)?.let { user ->
                        group.users.add(user.id)
                        user.listOfGroups.add(group.id)
                        firestore.collection("groups").document(uniqueId).set(group).addOnCompleteListener { collectionTask ->
                            if (collectionTask.isSuccessful) {
                                firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).set(user).addOnCompleteListener { userTask1 ->
                                    if (userTask1.isSuccessful) {
                                        firestore.collection("messages").document("${group.id} - messages").set(ListOfMessages()).addOnCompleteListener { messagesTask ->
                                            if (messagesTask.isSuccessful) {
                                                createGroupStatusLiveData.postValue(true)
                                            } else {
                                                createGroupStatusLiveData.postValue(false)
                                                messagesTask.exception?.let { createGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                            }
                                        }
                                    } else {
                                        createGroupStatusLiveData.postValue(false)
                                        userTask1.exception?.let { createGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                    }
                                }
                            } else {
                                createGroupStatusLiveData.postValue(false)
                                collectionTask.exception?.let { createGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                            }
                        }
                    }
                }
            } else {
                createGroupStatusLiveData.postValue(false)
                userTask.exception?.let { createGroupErrorMessageLiveData.postValue(it.localizedMessage) }
            }
        }
    }

    fun joinGroup(id: String) {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result?.let { snapshot ->
                    snapshot.toObject(User::class.java)?.let { user1 ->
                        if (id in user1.listOfGroups) {
                            joinGroupStatusLiveData.postValue(false)
                            joinGroupErrorMessageLiveData.postValue("You already belong to this group!")
                        } else {
                            firestore.collection("groups").whereEqualTo("id", id).get().addOnCompleteListener { collectionTask ->
                                if (collectionTask.isSuccessful) {
                                    collectionTask.result?.let { querySnapshot ->
                                        if (querySnapshot.isEmpty) {
                                            joinGroupStatusLiveData.postValue(false)
                                            joinGroupErrorMessageLiveData.postValue("There is no group with the id you provided!")
                                        } else {
                                            for (dc in querySnapshot.documents) {
                                                dc.toObject(Group::class.java)?.let { group ->
                                                    group.users.add(FirebaseAuth.getInstance().currentUser!!.uid.substring(0, 6))
                                                    firestore.collection("groups").document(dc.id).set(group).addOnCompleteListener { anotherTask ->
                                                        if (anotherTask.isSuccessful) {
                                                            firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).get().addOnCompleteListener { userTask ->
                                                                if (userTask.isSuccessful) {
                                                                    userTask.result?.let { documentSnapshot ->
                                                                        documentSnapshot.toObject(User::class.java)?.let { user ->
                                                                            user.listOfGroups.add(id)
                                                                            firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).set(user).addOnCompleteListener { secondUserTask ->
                                                                                if (secondUserTask.isSuccessful) {
                                                                                    joinGroupStatusLiveData.postValue(true)
                                                                                } else {
                                                                                    joinGroupStatusLiveData.postValue(false)
                                                                                    userTask.exception?.let { joinGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    joinGroupStatusLiveData.postValue(false)
                                                                    userTask.exception?.let { joinGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                                                }
                                                            }
                                                        } else {
                                                            joinGroupStatusLiveData.postValue(false)
                                                            anotherTask.exception?.let { joinGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    joinGroupStatusLiveData.postValue(false)
                                    collectionTask.exception?.let { joinGroupErrorMessageLiveData.postValue(it.localizedMessage) }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun registerGroupLiveListener() {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users").addSnapshotListener { value, error ->
            if (error != null) {
                Log.w("GROUPS_DATA_ERROR", error.localizedMessage!!)
            }
            value?.let {
                val groupList = ArrayList<Group>()
                val userUUID = FirebaseAuth.getInstance().currentUser!!.uid
                for (dc in it.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            if (dc.document.id == userUUID) {
                                val user = dc.document.toObject(User::class.java)
                                for (id in user.listOfGroups) {
                                    firestore.collection("groups").whereEqualTo("id", id).get().addOnCompleteListener { groupTask ->
                                        if (groupTask.isSuccessful) {
                                            groupTask.result?.let { querySnapshot ->
                                                for (doc in querySnapshot.documents) {
                                                    doc.toObject(Group::class.java)?.let { group -> groupList.add(group) }
                                                }
                                                groupListLiveData.postValue(groupList)
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        DocumentChange.Type.MODIFIED -> {
                            if (dc.document.id == userUUID) {
                                val user = dc.document.toObject(User::class.java)
                                for (id in user.listOfGroups) {
                                    firestore.collection("groups").whereEqualTo("id", id).get().addOnCompleteListener { groupTask ->
                                        if (groupTask.isSuccessful) {
                                            groupTask.result?.let { querySnapshot ->
                                                for (doc in querySnapshot.documents) {
                                                    doc.toObject(Group::class.java)?.let { group -> groupList.add(group) }
                                                }
                                                groupListLiveData.postValue(groupList)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        DocumentChange.Type.REMOVED -> Log.d("GROUP_REMOVED", dc.document.toString())
                    }
                }
            }
        }
    }

    init {
        registerGroupLiveListener()
    }
}