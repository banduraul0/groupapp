package com.example.groupapp.seeUserGroups

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R
import com.example.groupapp.dataClasses.Group
import com.example.groupapp.group.GroupActivity
import com.google.firebase.auth.FirebaseAuth

class SeeUserGroupsActivity : AppCompatActivity(), UserGroupsAdapter.GroupsListClickListener, LifecycleObserver {
    private lateinit var groupsRecyclerView: RecyclerView
    private lateinit var noGroupsTextView: TextView
    private lateinit var viewModel: SeeUserGroupsViewModel
    private lateinit var groupListObserver: Observer<ArrayList<Group>>
    private lateinit var joinGroupStatusObserver: Observer<Boolean>
    private lateinit var joinGroupErrorMessageObserver: Observer<String>
    private lateinit var createGroupStatusObserver: Observer<Boolean>
    private lateinit var createGroupErrorMessageObserver: Observer<String>
    private lateinit var adapter: UserGroupsAdapter
    private lateinit var createGroupBtn: Button
    private lateinit var joinGroupBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_user_groups)

        lifecycle.addObserver(this)

        viewModel = ViewModelProvider(this).get(SeeUserGroupsViewModel::class.java)

        groupsRecyclerView = findViewById(R.id.userGroups_recyclerview)
        groupsRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UserGroupsAdapter(this)
        groupsRecyclerView.adapter = adapter

        noGroupsTextView = findViewById(R.id.noGroupsTextView)

        createGroupBtn = findViewById(R.id.buttonCreateGroup)
        createGroupBtn.setOnClickListener {
            val groupNameEditText = EditText(this)
            groupNameEditText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Insert the name of the group you want to create")
                .setView(groupNameEditText)
                .setPositiveButton("Create") { _, _ -> }.create()
            alertDialog.show()
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                val text = groupNameEditText.text.toString().trim()
                if (text != "") {
                    viewModel.createGroup(text)
                    alertDialog.dismiss()
                } else {
                    Toast.makeText(this, "Group name can't be empty!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        joinGroupBtn = findViewById(R.id.buttonJoinGroup)
        joinGroupBtn.setOnClickListener {
            val groupIdEditText = EditText(this)
            groupIdEditText.inputType = InputType.TYPE_CLASS_TEXT
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Insert the id of the group you want to join")
                .setView(groupIdEditText)
                .setPositiveButton("Join") { _, _ -> }.create()
            alertDialog.show()
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                val text = groupIdEditText.text.toString().trim()
                if (text != "") {
                    viewModel.joinGroup(text)
                    alertDialog.dismiss()
                } else {
                    Toast.makeText(this, "Group id can't be empty!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        groupListObserver = Observer {
            adapter.setData(it)
            if (it.isEmpty()) {
                noGroupsTextView.visibility = View.VISIBLE
            } else {
                noGroupsTextView.visibility = View.INVISIBLE
            }
        }
        joinGroupStatusObserver = Observer {
            if (it) {
                Toast.makeText(this, "You have joined the group successfully!", Toast.LENGTH_SHORT).show()
            }
        }
        joinGroupErrorMessageObserver = Observer { Toast.makeText(this, viewModel.joinGroupErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }
        createGroupStatusObserver = Observer {
            if (it) {
                Toast.makeText(this, "You have created the group successfully!", Toast.LENGTH_SHORT).show()
            }
        }
        createGroupErrorMessageObserver = Observer { Toast.makeText(this, viewModel.createGroupErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.groupListLiveData.observe(this, groupListObserver)
        viewModel.joinGroupStatusLiveData.observe(this, joinGroupStatusObserver)
        viewModel.joinGroupErrorMessageLiveData.observe(this, joinGroupErrorMessageObserver)
        viewModel.createGroupStatusLiveData.observe(this, createGroupStatusObserver)
        viewModel.createGroupErrorMessageLiveData.observe(this, createGroupErrorMessageObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.groupListLiveData.removeObserver(groupListObserver)
        viewModel.joinGroupStatusLiveData.removeObserver(joinGroupStatusObserver)
        viewModel.joinGroupErrorMessageLiveData.removeObserver(joinGroupErrorMessageObserver)
        viewModel.createGroupStatusLiveData.removeObserver(createGroupStatusObserver)
        viewModel.createGroupErrorMessageLiveData.removeObserver(createGroupErrorMessageObserver)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        FirebaseAuth.getInstance().signOut()
    }

    override fun groupItemClicked(group: Group) {
        val intent = Intent(this, GroupActivity::class.java)
        intent.putExtra("GROUP_INFO", group)
        startActivity(intent)
    }
}