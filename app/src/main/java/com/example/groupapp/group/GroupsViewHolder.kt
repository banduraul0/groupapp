package com.example.groupapp.group

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R

class GroupsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val groupString: TextView = itemView.findViewById(R.id.groupString)
}