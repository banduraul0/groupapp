package com.example.groupapp.group

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R
import com.example.groupapp.dataClasses.Group
import com.example.groupapp.liveChat.LiveChatActivity

class GroupActivity : AppCompatActivity(), GroupAdapter.GroupsListClickListener {
    private lateinit var group: Group
    private lateinit var groupsRecyclerView: RecyclerView
    private lateinit var adapter: GroupAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        group = intent.getParcelableExtra("GROUP_INFO")!!
        title = group.name
        supportActionBar?.let {
            it.subtitle = "id: ${group.id}"
        }

        setContentView(R.layout.activity_group)

        groupsRecyclerView = findViewById(R.id.groups_recyclerview)
        groupsRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = GroupAdapter(this)
        groupsRecyclerView.adapter = adapter
    }

    override fun groupItemClicked(position: Int) {
        when(position) {
            0 -> {
                val intent = Intent(this, LiveChatActivity::class.java)
                intent.putExtra("GROUP_INFO", group)
                startActivity(intent)
            }
            else -> Toast.makeText(this, position.toString(), Toast.LENGTH_SHORT).show()
        }
    }
}