package com.example.groupapp.group

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R

class GroupAdapter(private val clickListener: GroupsListClickListener) : RecyclerView.Adapter<GroupsViewHolder>() {
    private val list: ArrayList<String> = arrayListOf("Live Chat", "Notes")

    interface GroupsListClickListener {
        fun groupItemClicked(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.groups_view_holder, parent, false)
        return GroupsViewHolder(view)
    }

    override fun onBindViewHolder(holder: GroupsViewHolder, position: Int) {
        holder.groupString.text = list[position]
        holder.itemView.setOnClickListener { clickListener.groupItemClicked(position) }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
