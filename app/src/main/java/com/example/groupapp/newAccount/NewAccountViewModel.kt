package com.example.groupapp.newAccount

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.groupapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class NewAccountViewModel: ViewModel() {
    val userNameLiveData = MutableLiveData<String>()
    val emailLiveData = MutableLiveData<String>()
    val passwordLiveData = MutableLiveData<String>()
    val responseStatusLiveData = MutableLiveData<Boolean>()
    val responseErrorMessageLiveData = MutableLiveData<String>()

    private val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun addNewUser() {
        firestore.collection("users").whereEqualTo("username", userNameLiveData.value!!)
            .get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    firebaseAuth.createUserWithEmailAndPassword(emailLiveData.value!!, passwordLiveData.value!!).addOnCompleteListener { authTask ->
                        if (authTask.isSuccessful) {
                            if (authTask.result != null && authTask.result!!.user != null) {
                                val user =
                                    User(
                                        username = userNameLiveData.value!!,
                                        email = emailLiveData.value!!,
                                        password = passwordLiveData.value!!,
                                        id = authTask.result!!.user!!.uid.substring(0, 6)
                                        )
                                val userId = authTask.result!!.user!!.uid

                                firestore.collection("users").document(userId).set(user)
                                    .addOnCompleteListener { collectionTask ->
                                        if (collectionTask.isSuccessful) {
                                            responseStatusLiveData.postValue(true)
                                        } else {
                                            responseStatusLiveData.postValue(false)
                                            collectionTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
                                        }
                                    }
                            }
                        } else {
                            responseStatusLiveData.postValue(false)
                            authTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
                        }
                    }
                } else {
                    responseStatusLiveData.postValue(false)
                    responseErrorMessageLiveData.postValue("A user with this username already exists!")
                }
            }
    }
}