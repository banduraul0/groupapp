package com.example.groupapp.newAccount

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.example.groupapp.R
import com.example.groupapp.databinding.ActivityNewAccountBinding

class NewAccountActivity : AppCompatActivity(), LifecycleObserver {
    private lateinit var viewModel: NewAccountViewModel
    private lateinit var binding: ActivityNewAccountBinding
    private lateinit var userNameObserver: Observer<String>
    private lateinit var emailObserver: Observer<String>
    private lateinit var passwordObserver: Observer<String>
    private lateinit var statusObserver: Observer<Boolean>
    private lateinit var errorObserver: Observer<String>
    private var userNameFlag = false
    private var emailFlag = false
    private var passwordFlag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.New_account)

        lifecycle.addObserver(this)

        viewModel = ViewModelProvider(this).get(NewAccountViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_account)
        binding.viewModel = viewModel
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.userNameLiveData.removeObserver(userNameObserver)
        viewModel.emailLiveData.removeObserver(emailObserver)
        viewModel.passwordLiveData.removeObserver(passwordObserver)
        viewModel.responseStatusLiveData.removeObserver(statusObserver)
        viewModel.responseErrorMessageLiveData.removeObserver(errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        userNameObserver = Observer { userNameFlag = it != "" }
        emailObserver = Observer { emailFlag = it != "" }
        passwordObserver = Observer { passwordFlag = it != "" }
        statusObserver = Observer {
            if (it) {
                onBackPressed()
            }
        }
        errorObserver = Observer { Toast.makeText(this, viewModel.responseErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.userNameLiveData.observe(this, userNameObserver)
        viewModel.emailLiveData.observe(this, emailObserver)
        viewModel.passwordLiveData.observe(this, passwordObserver)
        viewModel.responseStatusLiveData.observe(this, statusObserver)
        viewModel.responseErrorMessageLiveData.observe(this, errorObserver)
    }

    fun onNewAccountClick(view: View) {
        if (userNameFlag && emailFlag && passwordFlag) {
            viewModel.addNewUser()
        } else {
            Toast.makeText(this, "All fields are mandatory!", Toast.LENGTH_SHORT).show()
        }
    }
}