package com.example.groupapp.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.example.groupapp.R
import com.example.groupapp.databinding.ActivityLoginBinding
import com.example.groupapp.seeUserGroups.SeeUserGroupsActivity
import com.example.groupapp.newAccount.NewAccountActivity

class LoginActivity : AppCompatActivity(), LifecycleObserver {
    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding
    private lateinit var emailObserver: Observer<String>
    private lateinit var passwordObserver: Observer<String>
    private lateinit var statusObserver: Observer<Boolean>
    private lateinit var errorObserver: Observer<String>
    private var emailFlag = false
    private var passwordFlag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.welcome)

        lifecycle.addObserver(this)

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.viewModel = viewModel

        binding.buttonNewAccount.setOnClickListener { startActivity(Intent(this, NewAccountActivity::class.java)) }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.emailLiveData.removeObserver(emailObserver)
        viewModel.passwordLiveData.removeObserver(passwordObserver)
        viewModel.responseStatusLiveData.removeObserver(statusObserver)
        viewModel.responseErrorMessageLiveData.removeObserver(errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        emailObserver = Observer { emailFlag = it != "" }
        passwordObserver = Observer { passwordFlag = it != "" }
        statusObserver = Observer {
            if (it) {
                    startActivity(Intent(this, SeeUserGroupsActivity::class.java))
            }
        }
        errorObserver = Observer { Toast.makeText(this, viewModel.responseErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.emailLiveData.observe(this, emailObserver)
        viewModel.passwordLiveData.observe(this, passwordObserver)
        viewModel.responseStatusLiveData.observe(this, statusObserver)
        viewModel.responseErrorMessageLiveData.observe(this, errorObserver)
    }

    fun onLogInCLick(view: View) {
        if (emailFlag && passwordFlag) {
            viewModel.signInUser()
        } else {
            Toast.makeText(this, "All fields are mandatory!", Toast.LENGTH_SHORT).show()
        }
    }
}