package com.example.groupapp.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class LoginViewModel: ViewModel() {
    val emailLiveData = MutableLiveData<String>()
    val passwordLiveData = MutableLiveData<String>()
    val responseStatusLiveData = MutableLiveData<Boolean>()
    val responseErrorMessageLiveData = MutableLiveData<String>()

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun signInUser() {
        firebaseAuth.signInWithEmailAndPassword(emailLiveData.value!!, passwordLiveData.value!!).addOnCompleteListener { authTask ->
            if (authTask.isSuccessful) {
                if(authTask.result != null && authTask.result!!.user != null) {
                        responseStatusLiveData.postValue(true)
                }
            } else {
                responseStatusLiveData.postValue(false)
                authTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
            }
        }
    }
}