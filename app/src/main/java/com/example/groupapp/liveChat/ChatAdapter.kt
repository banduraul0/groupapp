package com.example.groupapp.liveChat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R
import com.example.groupapp.dataClasses.Message
import com.example.groupapp.databinding.ChatViewHolderBinding
import com.google.firebase.auth.FirebaseAuth
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {
    private val messages: ArrayList<Message> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val viewHolderBinding = DataBindingUtil.inflate<ChatViewHolderBinding>(
            LayoutInflater.from(parent.context), R.layout.chat_view_holder, parent, false)
        return ChatViewHolder(viewHolderBinding)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
       holder.init(messages[position])
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    fun setData(chatList: ArrayList<Message>) {
        messages.clear()
        messages.addAll(chatList)
        messages.sortedBy { it.time }
        notifyDataSetChanged()
    }

    inner class ChatViewHolder(private val binding: ChatViewHolderBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var message: Message

        fun init(message: Message) {
            this.message = message
            initLayout()
        }

        private fun initLayout() {
            binding.usernameTextView.text = message.user.username
            binding.messageTextView.text = message.text
            binding.timeTextView.text = LocalDateTime.ofInstant(message.time.toInstant(), ZoneId.of("GMT+03:00")).format(DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm")).toString()
            if (FirebaseAuth.getInstance().currentUser!!.uid.contains(message.user.id)) {
                binding.usernameTextView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                binding.messageTextView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                binding.timeTextView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
            }
        }
    }
}