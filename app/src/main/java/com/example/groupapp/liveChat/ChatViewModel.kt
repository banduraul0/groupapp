package com.example.groupapp.liveChat

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.groupapp.dataClasses.ListOfMessages
import com.example.groupapp.dataClasses.Message
import com.example.groupapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class ChatViewModel : ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val messageLiveData = MutableLiveData<String>()
    val messageListLiveData = MutableLiveData<ArrayList<Message>>(ArrayList())
    val sendMessageStatusLiveData = MutableLiveData<Boolean>()
    val sendMessageErrorMessageLiveData = MutableLiveData<String>()

    fun sendMessage() {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).get().addOnCompleteListener { userTask ->
            if (userTask.isSuccessful) {
                userTask.result?.let { doc ->
                    doc.toObject(User::class.java)?.let { user ->
                        firestore.collection("messages").document("${group.id} - messages").get().addOnCompleteListener { messageTask ->
                            if (messageTask.isSuccessful) {
                                messageTask.result?.let { documentSnapshot ->
                                    documentSnapshot.toObject(ListOfMessages::class.java)?.let { messages ->
                                        messages.messages.add(Message(user = user, text = messageLiveData.value!!))
                                        firestore.collection("messages").document("${group.id} - messages").set(messages).addOnCompleteListener { task ->
                                            if (task.isSuccessful) {
                                                sendMessageStatusLiveData.postValue(true)
                                            } else {
                                                sendMessageStatusLiveData.postValue(false)
                                                task.exception?.let { sendMessageErrorMessageLiveData.postValue(it.localizedMessage) }
                                            }
                                        }
                                    }
                                }
                            } else {
                                sendMessageStatusLiveData.postValue(false)
                                messageTask.exception?.let { sendMessageErrorMessageLiveData.postValue(it.localizedMessage) }
                            }
                        }
                    }
                }
            } else {
                sendMessageStatusLiveData.postValue(false)
                userTask.exception?.let { sendMessageErrorMessageLiveData.postValue(it.localizedMessage) }
            }
        }
    }

    private fun registerChatLiveListener() {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("messages").addSnapshotListener { value, error ->
            if (error != null) {
                Log.w("MESSAGE_DATA_ERROR", error.localizedMessage!!)
            }
            value?.let {
                val messageList = ArrayList<Message>()
                for (dc in it.documentChanges) {
                    if (dc.document.id == "${group.id} - messages") {
                        when(dc.type) {
                            DocumentChange.Type.ADDED, DocumentChange.Type.MODIFIED -> {
                                dc.document.toObject(ListOfMessages::class.java).let { listOfMessages ->
                                    messageList.addAll(listOfMessages.messages)
                                    messageListLiveData.postValue(messageList)
                                }
                            }
                            DocumentChange.Type.REMOVED -> Log.d("MESSAGE REMOVED", dc.document.toString())
                        }
                    }
                }
            }
        }
    }

    init {
        registerChatLiveListener()
    }
}