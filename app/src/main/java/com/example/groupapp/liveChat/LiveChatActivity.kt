package com.example.groupapp.liveChat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.groupapp.R
import com.example.groupapp.dataClasses.Group
import com.example.groupapp.dataClasses.Message
import com.example.groupapp.databinding.ActivityLiveChatBinding

lateinit var group: Group

class LiveChatActivity : AppCompatActivity(), LifecycleObserver {
    private lateinit var chatRecyclerView: RecyclerView
    private lateinit var binding: ActivityLiveChatBinding
    private lateinit var adapter: ChatAdapter
    private lateinit var viewModel: ChatViewModel
    private lateinit var noMessagesTextView: TextView
    private lateinit var messageListObserver: Observer<ArrayList<Message>>
    private lateinit var messageObserver: Observer<String>
    private lateinit var statusObserver: Observer<Boolean>
    private lateinit var errorObserver: Observer<String>
    private lateinit var sendMessageBtn: Button
    private var messageFlag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Live Chat"

        group = intent.getParcelableExtra("GROUP_INFO")!!

        lifecycle.addObserver(this)

        viewModel = ViewModelProvider(this).get(ChatViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_live_chat)
        binding.viewModel = viewModel

        chatRecyclerView = findViewById(R.id.messagesRecyclerView)
        chatRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ChatAdapter()
        chatRecyclerView.adapter = adapter

        noMessagesTextView = findViewById(R.id.noMessagesTextView)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        messageObserver = Observer { messageFlag = it != "" }
        messageListObserver = Observer {
            adapter.setData(it)
            if (it.isEmpty()) {
                noMessagesTextView.visibility = View.VISIBLE
            } else {
                noMessagesTextView.visibility = View.INVISIBLE
            }
        }
        statusObserver = Observer {
            if (it) {
                Toast.makeText(this, "Message sent successfully!", Toast.LENGTH_SHORT).show()
            }
        }
        errorObserver = Observer { Toast.makeText(this, viewModel.sendMessageErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.messageLiveData.observe(this, messageObserver)
        viewModel.messageListLiveData.observe(this, messageListObserver)
        viewModel.sendMessageStatusLiveData.observe(this, statusObserver)
        viewModel.sendMessageErrorMessageLiveData.observe(this, errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.messageLiveData.removeObserver(messageObserver)
        viewModel.messageListLiveData.removeObserver(messageListObserver)
        viewModel.sendMessageStatusLiveData.removeObserver(statusObserver)
        viewModel.sendMessageErrorMessageLiveData.removeObserver(errorObserver)
    }

    fun onSendClick(view: View) {
        if (messageFlag) {
            viewModel.sendMessage()
        } else {
            Toast.makeText(this, "You can't send an empty message!", Toast.LENGTH_SHORT).show()
        }
    }
}